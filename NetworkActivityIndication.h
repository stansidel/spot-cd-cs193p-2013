//
//  NetworkActivityIndication.h
//  SPoT
//
//  Created by stan on 20/08/15.
//  Copyright © 2015 Stanislav Sidelnikov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetworkActivityIndication : NSObject

+ (void)startIndicator;
+ (void)stopIndicator;

@end
