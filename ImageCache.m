//
//  ImageCache.m
//  SPoT
//
//  Created by stan on 21/08/15.
//  Copyright © 2015 Stanislav Sidelnikov. All rights reserved.
//

#import "ImageCache.h"
#import "NetworkActivityIndication.h"

@implementation ImageCache

+ (NSData *)getImageByURL:(NSURL *)imageUrl
{
    NSData *imageData = nil;
    if (imageUrl) {
        NSString *fileName = [imageUrl.pathComponents lastObject];
        imageData = [self imageFromCache:fileName];
        
        if (!imageData) {
            [NetworkActivityIndication startIndicator];
            imageData = [[NSData alloc] initWithContentsOfURL:imageUrl];
            [NetworkActivityIndication stopIndicator];
            [self cacheImage:imageData withFilename:fileName];
        }
    }
    
    return imageData;
}

#pragma mark - Caching methods

+ (NSString *)imagesCachePath
{
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    
    NSString *imagesPath = nil;
    if ([paths count] == 1) {
        imagesPath = [[paths objectAtIndex:0] stringByAppendingString:@"/images/"];
        
        BOOL isDir;
        if (![fileManager fileExistsAtPath:imagesPath isDirectory:&isDir] || !isDir) {
            NSError *error;
            BOOL imageCacheAvailable = [fileManager createDirectoryAtPath:imagesPath
                                         withIntermediateDirectories:YES
                                                          attributes:nil
                                                               error:&error];
            if (!imageCacheAvailable) {
                imagesPath = nil;
                NSLog(@"Got an error while getting images cache: %@", error);
            }
        }
    }
    
    return imagesPath;
}


+ (NSData *)imageFromCache:(NSString *)fileName
{
    NSData *imageData = nil;
    
    if (fileName) {
        NSString *cachePath = [self imagesCachePath];
        
        if (cachePath) {
            NSFileManager *fileManager = [[NSFileManager alloc] init];
            NSString *fullFilePath = [cachePath stringByAppendingString:fileName];
            
            if ([fileManager isReadableFileAtPath:fullFilePath]) {
                imageData = [NSData dataWithContentsOfFile:fullFilePath];
            }
        }
    }
    
    return imageData;
}

/**
 * Estimated maximum count of images' caches. Used to calculate max total cache size.
 */
#define IMAGE_CACHE_MAX_COUNT 5

+ (void)cacheImage:(NSData *)imageData withFilename:(NSString *)fileName
{
    NSString *cachePath = [self imagesCachePath];
    
    if (cachePath) {
        NSFileManager *fileManager = [[NSFileManager alloc] init];
        
        NSArray *cachedImages = [self getSortedFilesFromFolder:cachePath withPredicate:nil];
        if (cachedImages.count >= IMAGE_CACHE_MAX_COUNT) {
            NSError *error;
            for (int i = IMAGE_CACHE_MAX_COUNT - 1; i < cachedImages.count; i++) {
                NSString *fileNameToDelete = [cachePath stringByAppendingString:cachedImages[i][@"path"]];
                if (![fileManager removeItemAtPath:fileNameToDelete error:&error]) {
                    NSLog(@"Error when deleting file \"%@\": %@", fileNameToDelete, error);
                }
            }
        }
        
        NSString *fullFilePath = [cachePath stringByAppendingString:fileName];
        if ([fileManager isWritableFileAtPath:fullFilePath] || ![fileManager fileExistsAtPath:fullFilePath]) {
            [imageData writeToFile:fullFilePath atomically:YES];
        }
    }
}

//This is reusable method which takes folder path and returns sorted file list
+ (NSArray*)getSortedFilesFromFolder:(NSString*)folderPath withPredicate:(NSPredicate *)predicate
{
    NSError *error = nil;
    NSArray* filesArray = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:folderPath error:&error];
    if (predicate) {
        filesArray =  [filesArray filteredArrayUsingPredicate:predicate];
    }
    
    // sort by creation date
    NSMutableArray* filesAndProperties = [NSMutableArray arrayWithCapacity:[filesArray count]];
    
    for(NSString* file in filesArray) {
        
        if (![file isEqualToString:@".DS_Store"]) {
            NSString* filePath = [folderPath stringByAppendingPathComponent:file];
            NSDictionary* properties = [[NSFileManager defaultManager]
                                        attributesOfItemAtPath:filePath
                                        error:&error];
            NSDate* modDate = [properties objectForKey:NSFileModificationDate];
            
            [filesAndProperties addObject:[NSDictionary dictionaryWithObjectsAndKeys:
                                           file, @"path",
                                           modDate, @"lastModDate",
                                           nil]];
            
        }
    }
    
    // Sort using a block - order inverted as we want latest date first
    NSArray* sortedFiles = [filesAndProperties sortedArrayUsingComparator:
                            ^(id path1, id path2)
                            {
                                // compare
                                NSComparisonResult comp = [[path1 objectForKey:@"lastModDate"] compare:
                                                           [path2 objectForKey:@"lastModDate"]];
                                // invert ordering
                                if (comp == NSOrderedDescending) {
                                    comp = NSOrderedAscending;
                                }
                                else if(comp == NSOrderedAscending){
                                    comp = NSOrderedDescending;
                                }
                                return comp;
                            }];
    
    return sortedFiles;
    
}


@end
