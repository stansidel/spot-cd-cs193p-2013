//
//  PhotosForTagCDTVC.h
//  SPoT CD
//
//  Created by stan on 28/08/15.
//  Copyright © 2015 GLoSS. All rights reserved.
//

#import "CoreDataTableViewController.h"
#import "PhotosCDTVC.h"
#import "Tag.h"

@interface PhotosForTagCDTVC : PhotosCDTVC

@property (strong, nonatomic) Tag *tag;

@end
