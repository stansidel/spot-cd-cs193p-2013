//
//  PhotosCDTVC.h
//  SPoT CD
//
//  Created by stan on 28/08/15.
//  Copyright © 2015 GLoSS. All rights reserved.
//

#import "CoreDataTableViewController.h"

@interface PhotosCDTVC : CoreDataTableViewController

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
- (void)setupFetchedResultsController;

- (void)updateFetchRequest:(NSFetchRequest *)request; // abstract
- (BOOL)shouldUpdatePhotoViewDate; // abstract

@end
