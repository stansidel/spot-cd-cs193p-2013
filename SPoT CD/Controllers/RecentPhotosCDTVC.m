//
//  RecentPhotosCDTVC.m
//  SPoT CD
//
//  Created by stan on 28/08/15.
//  Copyright © 2015 GLoSS. All rights reserved.
//

#import "RecentPhotosCDTVC.h"

@interface RecentPhotosCDTVC ()

@end

@implementation RecentPhotosCDTVC

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setContext];
}

- (void)setContext
{
    NSURL *url = [[[NSFileManager defaultManager] URLsForDirectory:NSCachesDirectory inDomains:NSUserDomainMask] lastObject];
    url = [url URLByAppendingPathComponent:@"Stanford Photos"];
    UIManagedDocument *document = [[UIManagedDocument alloc] initWithFileURL:url];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:[url path]]) {
        [document saveToURL:url
           forSaveOperation:UIDocumentSaveForCreating
          completionHandler:^(BOOL success) {
              if (success) {
                  self.managedObjectContext = document.managedObjectContext;
                  [self setupFetchedResultsController];
              }
          }];
    } else if (document.documentState == UIDocumentStateClosed) {
        [document openWithCompletionHandler:^(BOOL success) {
            if (success) {
                self.managedObjectContext = document.managedObjectContext;
                [self setupFetchedResultsController];
            }
        }];
    } else {
        self.managedObjectContext = document.managedObjectContext;
        [self setupFetchedResultsController];
    }

}

#pragma mark - Abstract

#define RECENT_PHOTOS_COUNT 20

- (void)updateFetchRequest:(NSFetchRequest *)request
{
    request.predicate = [NSPredicate predicateWithFormat:@"lastViewed != nil"];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"lastViewed"
                                                              ascending:NO]];
    request.fetchLimit = RECENT_PHOTOS_COUNT;
}

@end
