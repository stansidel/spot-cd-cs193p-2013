//
//  PhotosForTagCDTVC.m
//  SPoT CD
//
//  Created by stan on 28/08/15.
//  Copyright © 2015 GLoSS. All rights reserved.
//

#import "PhotosForTagCDTVC.h"

@interface PhotosForTagCDTVC ()

@end

@implementation PhotosForTagCDTVC

#pragma mark - Properties

- (void)setTag:(Tag *)tag
{
    _tag = tag;
    self.managedObjectContext = tag.managedObjectContext;
    [self setupFetchedResultsController];
}

#pragma mark - Abstract

- (void)updateFetchRequest:(NSFetchRequest *)request
{
    request.predicate = [NSPredicate predicateWithFormat:@"ANY tags = %@", self.tag];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"title"
                                                              ascending:YES
                                                               selector:@selector(localizedCaseInsensitiveCompare:)]];
}

- (BOOL)shouldUpdatePhotoViewDate
{
    return YES;
}

@end
