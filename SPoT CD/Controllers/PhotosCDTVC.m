//
//  PhotosCDTVC.m
//  SPoT CD
//
//  Created by stan on 28/08/15.
//  Copyright © 2015 GLoSS. All rights reserved.
//

#import "PhotosCDTVC.h"
#import "Photo.h"
#import "NetworkActivityIndication.h"

@interface PhotosCDTVC ()

@end

@implementation PhotosCDTVC



#pragma mark - Database

- (void)setupFetchedResultsController
{
    if (self.managedObjectContext) {
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Photo"];
        [self updateFetchRequest:request];
        self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    } else {
        self.fetchedResultsController = nil;
    }
}

#pragma mark - UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Photo" forIndexPath:indexPath];
    
    Photo *photo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.textLabel.text = photo.title;
    cell.detailTextLabel.text = photo.userDescription;
    
    if (photo.thumbnail) {
        cell.imageView.image = [UIImage imageWithData:photo.thumbnail];
    } else {
        dispatch_queue_t loadThumbQ = dispatch_queue_create("Load Thumbnail", NULL);
        dispatch_async(loadThumbQ, ^{
            if (photo.thumbnailURL) {
                NSURL *imageUrl = [NSURL URLWithString:photo.thumbnailURL];
                [NetworkActivityIndication startIndicator];
                NSData *imageData = [[NSData alloc] initWithContentsOfURL:imageUrl];
                [NetworkActivityIndication stopIndicator];
                if (imageData) {
                    if (photo && self.managedObjectContext) {
                        [self.managedObjectContext performBlock:^{
                            photo.thumbnail = imageData;
                        }];
                    }
                }
            }
        });
    }
    
    return cell;
}

#pragma mark - Segueing

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSIndexPath *indexPath = nil;
    
    if ([sender isKindOfClass:[UITableViewCell class]]) {
        indexPath = [self.tableView indexPathForCell:sender];
    }
    
    if (indexPath) {
        if ([segue.identifier isEqualToString:@"setPhotoUrl:"]) {
            Photo *photo = [self.fetchedResultsController objectAtIndexPath:indexPath];
            NSURL *photoUrl = [NSURL URLWithString:[photo photoURL]];
            if ([segue.destinationViewController respondsToSelector:@selector(setPhotoURL:)]) {
                [segue.destinationViewController performSelector:@selector(setPhotoURL:) withObject:photoUrl];
                if ([self shouldUpdatePhotoViewDate]) {
                    photo.lastViewed = [NSDate date];
                }
            }
        }
    }
}

#pragma mark - Abstract methods

- (BOOL)shouldUpdatePhotoViewDate
{
    return NO;
}

- (void)updateFetchRequest:(NSFetchRequest *)request
{
    
}
@end
