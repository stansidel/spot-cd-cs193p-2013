//
//  Photo.m
//  SPoT CD
//
//  Created by stan on 27/08/15.
//  Copyright © 2015 GLoSS. All rights reserved.
//

#import "Photo.h"
#import "Tag.h"

@implementation Photo

+ (NSArray *)ignoredTags
{
    return @[@"cs193pspot", @"portrait", @"landscape"];
}

+ (Photo *)photoWithFlickrData:(NSDictionary *)flickrData
               forFlickrFormat:(FlickrPhotoFormat)format
              inManagedContext:(NSManagedObjectContext *)context
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Photo"];
    request.predicate = [NSPredicate predicateWithFormat:@"flickrID = %@", flickrData[FLICKR_PHOTO_ID]];
    NSError *error;
    
    NSArray *photos = [context executeFetchRequest:request error:&error];
    
    if (photos == nil) {
        NSLog(@"Error fetching photo for flickrID %@. Error: %@", flickrData[FLICKR_PHOTO_ID], error);
    }
    
    Photo *photo = [photos lastObject];
    
    if (!photo) {
        photo = [NSEntityDescription insertNewObjectForEntityForName:@"Photo" inManagedObjectContext:context];
        
        photo.title = flickrData[FLICKR_PHOTO_TITLE];
        photo.flickrID = flickrData[FLICKR_PHOTO_ID];
        photo.userDescription = [flickrData valueForKeyPath:FLICKR_PHOTO_DESCRIPTION];
        photo.photoURL = [[FlickrFetcher urlForPhoto:flickrData
                                              format:format] description];
        photo.thumbnailURL = [[FlickrFetcher urlForPhoto:flickrData
                                                  format:FlickrPhotoFormatSquare] description];
        
        NSArray *tags = [flickrData[FLICKR_TAGS] componentsSeparatedByString:@" "];
        for (NSString *tagName in tags) {
            if (![[self ignoredTags] containsObject:tagName]) {
                Tag *tag = [Tag tagWithName:tagName inManagedContext:context];
                
                if (tag) {
                    [photo addTagsObject:tag];
                }
            }
        }
    }
    
    return photo;
}

@end
