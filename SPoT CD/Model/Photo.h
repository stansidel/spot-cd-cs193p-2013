//
//  Photo.h
//  SPoT CD
//
//  Created by stan on 27/08/15.
//  Copyright © 2015 GLoSS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "FlickrFetcher.h"
#import "Tag.h"

NS_ASSUME_NONNULL_BEGIN

@interface Photo : NSManagedObject

+ (Photo *)photoWithFlickrData:(NSDictionary *)flickrData forFlickrFormat:(FlickrPhotoFormat)format inManagedContext:(NSManagedObjectContext *)context;

@end

NS_ASSUME_NONNULL_END

#import "Photo+CoreDataProperties.h"
