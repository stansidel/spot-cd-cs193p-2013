//
//  Tag.h
//  SPoT CD
//
//  Created by stan on 27/08/15.
//  Copyright © 2015 GLoSS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Photo;

NS_ASSUME_NONNULL_BEGIN

@interface Tag : NSManagedObject

+ (Tag *)tagWithName:(NSString *)name
    inManagedContext:(NSManagedObjectContext *)context;

@end

NS_ASSUME_NONNULL_END

#import "Tag+CoreDataProperties.h"
