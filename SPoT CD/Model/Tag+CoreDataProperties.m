//
//  Tag+CoreDataProperties.m
//  SPoT CD
//
//  Created by stan on 27/08/15.
//  Copyright © 2015 GLoSS. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Tag+CoreDataProperties.h"

@implementation Tag (CoreDataProperties)

@dynamic name;
@dynamic displayName;
@dynamic photos;

@end
