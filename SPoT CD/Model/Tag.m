//
//  Tag.m
//  SPoT CD
//
//  Created by stan on 27/08/15.
//  Copyright © 2015 GLoSS. All rights reserved.
//

#import "Tag.h"
#import "Photo.h"

@implementation Tag

+ (Tag *)tagWithName:(NSString *)name
    inManagedContext:(nonnull NSManagedObjectContext *)context
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Tag"];
    request.predicate = [NSPredicate predicateWithFormat:@"name = %@", name];
    NSError *error;
    
    NSArray *tags = [context executeFetchRequest:request error:&error];
    
    if (tags == nil) {
        NSLog(@"Error fetching tags for name %@. Error: %@.", name, error);
    }
    
    Tag *tag = [tags lastObject];
    
    if (!tag) {
        tag = [NSEntityDescription insertNewObjectForEntityForName:@"Tag"
                                            inManagedObjectContext:context];
        
        tag.name = name;
        tag.displayName = [name capitalizedString];
    }
    
    return tag;
}

@end
