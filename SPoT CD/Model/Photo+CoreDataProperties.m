//
//  Photo+CoreDataProperties.m
//  SPoT CD
//
//  Created by Stanislav Sidelnikov on 29/08/15.
//  Copyright © 2015 GLoSS. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Photo+CoreDataProperties.h"

@implementation Photo (CoreDataProperties)

@dynamic flickrID;
@dynamic lastViewed;
@dynamic photoURL;
@dynamic title;
@dynamic userDescription;
@dynamic thumbnail;
@dynamic thumbnailURL;
@dynamic tags;

@end
